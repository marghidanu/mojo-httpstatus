FROM boord/perl:5.30

WORKDIR /opt/app

COPY . .

RUN cpanm -nq Module::Build

RUN perl Build.PL \
&& ./Build installdeps --cpan_client="cpanm -nq" \
&& ./Build manifest \
&& ./Build 
