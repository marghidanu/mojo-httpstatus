# mojo-httpstatus

## Build status

[![pipeline status](https://gitlab.com/marghidanu/mojo-httpstatus/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/mojo-httpstatus/commits/master)

## Description

Readable HTTP status codes

## Development environment

    docker-compose build
    docker-compose run --rm app prove -b
